export interface IDropdownItem {
    id: any;
    description: string;
  }

export class DropdownItem implements IDropdownItem {

    public id: any;

    public description: string;

    constructor(init?: Partial<IDropdownItem>) {
      this.id = 0;
      this.description = '';
      Object.assign(this, init);
    }
  }
