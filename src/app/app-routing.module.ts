import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SnowboardsModule } from './snowboards/snowboards.module';

const routes: Routes = [
  {
    path: '',
    loadChildren: './snowboards/snowboards.module#SnowboardsModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes), SnowboardsModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
