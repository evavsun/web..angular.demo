import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SnowboardsListComponent } from './snowboards-list/snowboards-list.component';
import { SnowboardFromComponent } from './snowboard-from/snowboard-from.component';
import { SnowboardFormResolver } from './snowboard-from/resolvers/SnowboardFormResolver';

const routes: Routes = [
  {
    path: '',
    component: SnowboardsListComponent
  },
  {
    path: 'create',
    component: SnowboardFromComponent,
    resolve: { model: SnowboardFormResolver }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class SnowboardsRoutingModule { }
