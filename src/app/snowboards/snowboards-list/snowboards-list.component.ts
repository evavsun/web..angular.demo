import { Component, OnInit } from '@angular/core';
import { SnowboardsClient, SnowboardDetailsRes } from 'src/shared/services/apiClient';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-snowboards-list',
  templateUrl: './snowboards-list.component.html',
  styleUrls: ['./snowboards-list.component.scss']
})
export class SnowboardsListComponent implements OnInit {

  public asyncSnowboards: Observable<SnowboardDetailsRes[]>;
  public snowboards: SnowboardDetailsRes[];

  constructor(private snowboardClient: SnowboardsClient) { }

  ngOnInit() {
    this.asyncSnowboards = this.snowboardClient.getSnowboards();
    this.asyncSnowboards.subscribe((data) => {
      this.snowboards = data;
    });
  }

}
