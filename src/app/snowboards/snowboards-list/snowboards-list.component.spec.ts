import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SnowboardsListComponent } from './snowboards-list.component';

describe('SnowboardsListComponent', () => {
  let component: SnowboardsListComponent;
  let fixture: ComponentFixture<SnowboardsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SnowboardsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SnowboardsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
