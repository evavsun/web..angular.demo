import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SnowboardFromComponent } from './snowboard-from.component';

describe('SnowboardFromComponent', () => {
  let component: SnowboardFromComponent;
  let fixture: ComponentFixture<SnowboardFromComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SnowboardFromComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SnowboardFromComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
