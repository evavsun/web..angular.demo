import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { KeyValuePairRes, FirmsClient } from 'src/shared/services/apiClient';
import { Observable } from 'rxjs/internal/Observable';

@Injectable()
export class SnowboardFormResolver implements Resolve<KeyValuePairRes[]> {
  constructor(private companiesClient: FirmsClient, private router: Router) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<KeyValuePairRes[]> {
    return this.companiesClient.getDropdownCompanies();
  }
}
