import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { KeyValuePairRes, SnowboardsClient } from 'src/shared';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DropdownItem } from 'src/shared/models/commonModels/dropdownModel';

@Component({
  selector: 'app-snowboard-from',
  templateUrl: './snowboard-from.component.html',
  styleUrls: ['./snowboard-from.component.scss']
})
export class SnowboardFromComponent implements OnInit {

  public form: FormGroup;
  public companies: KeyValuePairRes[] = [];
  public dropdownItems: DropdownItem[];
  public isSubmited = false;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private fb: FormBuilder,
              private snowboardClient: SnowboardsClient) {
    this.route.data.subscribe((data) => {
      if (data.model) {
        this.companies = data.model;
        this.dropdownItems = this.companies.map(f => {
          return  new DropdownItem({id: f.key, description: f.value});
        });
      }
    });
   }

  ngOnInit() {
    this.form = this.createForm();
  }

  onSubmit() {
    this.isSubmited = true;
    if (this.form.invalid) {
      return;
    }

    this.snowboardClient.createSnowboard(this.form.value)
    .subscribe((_) => {
      this.router.navigate(['../']);
    });
  }

  private createForm() {
    return this.fb.group({
        type: ['', Validators.required],
        color: ['', Validators.pattern('#[0-9A-F]{6}$')],
        firmId: ['', Validators.required],
      });
  }

  selectionChanged(dropdownItem) {
    this.form.controls.firmId.setValue(dropdownItem.value.id);
  }

  get type() { return this.form.get('type'); }
  get color() { return this.form.get('color'); }
  get firmId() { return this.form.get('firmId'); }

}
