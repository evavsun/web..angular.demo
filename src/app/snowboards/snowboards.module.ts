import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SnowboardsListComponent } from './snowboards-list/snowboards-list.component';
import { SnowboardFromComponent } from './snowboard-from/snowboard-from.component';
import { SnowboardsRoutingModule } from './snowboards.route.module';
import { SnowboardsClient, FirmsClient } from 'src/shared/services/apiClient';
import { SelectDropDownModule } from 'ngx-select-dropdown';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { SnowboardFormResolver } from './snowboard-from/resolvers/SnowboardFormResolver';
import { ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [SnowboardsListComponent, SnowboardFromComponent],
  imports: [
    CommonModule,
    SnowboardsRoutingModule,
    MDBBootstrapModule,
    SelectDropDownModule,
    ReactiveFormsModule,
  ],
  exports: [SnowboardsListComponent],
  providers: [SnowboardsClient, FirmsClient, SnowboardFormResolver]
})
export class SnowboardsModule { }
